// bài 1
document.getElementById("btn-ket-qua-1").addEventListener("click",function(){
    var diemChuanValue = document.getElementById("txt-diem-chuan").value*1;
    var diemSo1Value = document.getElementById("txt-diem-mon-1").value*1;
    var diemSo2Value = document.getElementById("txt-diem-mon-2").value*1;
    var diemSo3Value = document.getElementById("txt-diem-mon-3").value*1;
    var khuVucValue = document.getElementById("list-1").value*1;
    var doiTuongValue = document.getElementById("list-2").value*1;
    var sum = diemSo1Value+diemSo2Value+diemSo3Value+khuVucValue+doiTuongValue;

    if(diemSo1Value==0 || diemSo2Value==0 || diemSo3Value==0){
        console.log("Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0");
    }else if(sum>=diemChuanValue){
        console.log("Bạn đã đậu.","Tổng điểm",sum);
    }
    else{
        console.log("Bạn đã rớt.","Tổng điểm",sum);
    }
});

// bài 2

document.getElementById("btn-ket-qua-2").addEventListener("click",function(){
    var hoTenValue = document.getElementById("txt-ho-ten").value;
    var soKwValue = document.getElementById("txt-so-kw").value*1;
    var soTienPhaiTra=0;

    const soTien50KwDau = 500;
    const soTien50kwKe = 650;
    const soTien100KwKe = 850;
    const soTien150KwKe = 1100;
    const soTienConLai = 1300;

    if(soKwValue<=50){
        soTienPhaiTra=soKwValue*soTien50KwDau;
        console.log("Họ tên:",hoTenValue,";","Tiền điện:",soTienPhaiTra);
    }else if(soKwValue<=100){
        soTienPhaiTra=(50*soTien50KwDau)+(soKwValue-50)*soTien50kwKe;
        console.log("Họ tên:",hoTenValue,";","Tiền điện:",soTienPhaiTra);
    }else if(soKwValue<=200){
        soTienPhaiTra=(50*soTien50KwDau)+(50*soTien50kwKe)+(soKwValue-100)*soTien100KwKe;
        console.log("Họ tên:",hoTenValue,";","Tiền điện:",soTienPhaiTra);
    }else if(soKwValue<=350){
        soTienPhaiTra=(50*soTien50KwDau)+(50*soTien50kwKe)+(100*soTien100KwKe)+(soKwValue-200)*soTien150KwKe;
        console.log("Họ tên:",hoTenValue,";","Tiền điện:",soTienPhaiTra);
    }
    else{
        soTienPhaiTra=(50*soTien50KwDau)+(50*soTien50kwKe)+(100*soTien100KwKe)+(150*soTien150KwKe)+(soKwValue-350)*soTienConLai;
        console.log("Họ tên:",hoTenValue,";","Tiền điện:",soTienPhaiTra);
    }
});
